package com.example.fuzzproductionsapptest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;


import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class MainActivity extends ActionBarActivity
{
	static DataAdapter adapter;
	static ArrayList<DataModel> dataModel;
	static ListView listview;
	static String selectedListItem = "all";
	public ImageLoader imageLoader;
	public Context mContext = this;
	public DisplayImageOptions options;
	RadioGroup rg;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar_id));
		listview = (ListView)findViewById(R.id.listAll);
		dataModel = new ArrayList<DataModel>();
		rg = ((RadioGroup)findViewById(R.id.rg));

		rg.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			public void onCheckedChanged(RadioGroup rg, int checkedId)
			{	
				
				if (adapter != null){
					adapter.clear();
				} 
				switch (checkedId){

				case R.id.allRB:
					selectedListItem = "all";
					new DataAsyncTask().execute("http://dev.fuzzproductions.com/MobileTest/test.json");
					adapter = new DataAdapter(mContext, R.layout.list_all, dataModel);
					listview.setAdapter(adapter);
					return;
				case R.id.textRB:
					selectedListItem = "text";
					listview = (ListView)findViewById(R.id.listAll);
					new DataAsyncTask().execute("http://dev.fuzzproductions.com/MobileTest/test.json");
					adapter = new DataAdapter(mContext, R.layout.list_all, dataModel);
					listview.setAdapter(adapter);
					return;
				case R.id.imageRB:

					selectedListItem = "image";
					new DataAsyncTask().execute("http://dev.fuzzproductions.com/MobileTest/test.json" );
					adapter = new DataAdapter(mContext, R.layout.list_all, dataModel);
					listview.setAdapter(adapter);

				default:
					return;
				}
			}
		});

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.resetViewBeforeLoading(true)
				/*.showImageForEmptyUri(R.drawable.error404)*/
				.showImageOnFail(R.drawable.error404)
				.showImageOnLoading(R.drawable.loadingwait)
				.bitmapConfig(Bitmap.Config.ARGB_4444)
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.defaultDisplayImageOptions(defaultOptions)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				//	.memoryCacheSize(20 * 1024 * 1024) // 20 Mb
				.memoryCache(new LruMemoryCache(20 * 1024 * 1024))
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.memoryCache(new WeakMemoryCache())
				.threadPoolSize(1)
				.build();

		ImageLoader.getInstance().init(config);

		//ASYNC TASK for main URL

		new DataAsyncTask().execute("http://dev.fuzzproductions.com/MobileTest/test.json");
		adapter = new DataAdapter(getApplicationContext(), R.layout.list_all, dataModel);
		listview.setAdapter(adapter);


		listview.setOnItemClickListener(new OnItemClickListener() {
			boolean isImageFitToScreen = true;
			ImageView image = new ImageView(mContext);

			public void onItemClick(AdapterView<?> adapterView, View view, int position,
					long id) {
				LinearLayout linearLayoutParent = (LinearLayout) view;
				image= (ImageView) linearLayoutParent.getChildAt(4);
				
				//For Text click
				final TextView clickOnText = (TextView) linearLayoutParent.getChildAt(3);
				TextView dataTypeClicked = (TextView) linearLayoutParent.getChildAt(2);

				if(dataTypeClicked.getText().toString().equals("text") || dataTypeClicked.getText().toString().equals("other")){
					if(clickOnText != null){
						String url = "https://fuzzproductions.com/";
						Intent fuzzUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
						startActivity(fuzzUrlIntent);
					}  

				}else if (dataTypeClicked.getText().toString().equals("image")){

					if(image != null ){
						if(isImageFitToScreen) {
							isImageFitToScreen=false;
							image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
							image.setAdjustViewBounds(true);
						}else{
							isImageFitToScreen=true;

							/* image.setLayoutParams(new LinearLayout.LayoutParams(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN));

			                    image.setScaleType(ImageView.ScaleType.FIT_XY);
							 */

							image.buildDrawingCache();
							Bitmap bitmapImage= image.getDrawingCache();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, baos); 
							byte[] b = baos.toByteArray();

							Intent intent = new Intent(getApplicationContext(), FullImage.class);
							intent.putExtra("picture", b);
							startActivity(intent);	                

						}	

					} 		
				}

			}

		});	

	}




//Async Task class
	class DataAsyncTask extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... urls) {
			try {

				HttpClient client = new DefaultHttpClient();
				HttpGet post = new HttpGet(urls[0]);
				HttpResponse response = client.execute(post);

				int status = response.getStatusLine().getStatusCode();

				if (status == 200) {
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);

					JSONArray jsonArray = new JSONArray(data);


					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						DataModel dataList = new DataModel();

						if ((object.getString("type").equalsIgnoreCase(selectedListItem)) || (selectedListItem.equalsIgnoreCase("all"))){

							dataList.setDataId(object.getString("id"));
							if(object.has("date")){
								dataList.setDataDate(object.getString("date"));
							}
							dataList.setDataType(object.getString("type"));

							if(object.has("data")){
								dataList.setDataDesc(null);
								dataList.setDataImg(null);
								if(object.getString("type").equalsIgnoreCase("text")){
									dataList.setDataDesc(object.getString("data"));	

									if(dataList.getDataDesc().isEmpty()){
										dataList.setDataDesc("Null Data");
									}
								}else if((object.getString("type").equalsIgnoreCase("image"))){
									dataList.setDataImg(object.getString("data"));
								}else if((object.getString("type").equalsIgnoreCase("other"))){ 
									dataList.setDataDesc(object.getString("data"));
								}else{
									dataList.setDataImg(null);
								}

							}else if(!(object.has("data"))) {
								dataList.setDataImg(null);
								dataList.setDataDesc("No Data column");
							}
							dataModel.add(dataList);
						}
					}	
					return true;
				}

			} catch (ParseException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return false;
		}

		protected void onPostExecute(Boolean result) {
			adapter.notifyDataSetChanged();
		}

	}
}	
