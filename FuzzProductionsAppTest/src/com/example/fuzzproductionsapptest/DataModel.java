package com.example.fuzzproductionsapptest;

public class DataModel {
	
	private String dataId;
	private String dataType;
	private String dataDate;
	private String dataDesc;
	private String dataImg;
	
	public String getDataImg() {
		return dataImg;
	}

	public void setDataImg(String dataImg) {
		this.dataImg = dataImg;
	}

	public DataModel(){
		
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		if(dataId != null){
			this.dataId = dataId;
		}else{
			this.dataId = "null";
		}
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		if(dataType != null){
			this.dataType = dataType;
		}else{
			this.dataType = "null";
		}	}

	public String getDataDate() {
		return dataDate;
	}

	public void setDataDate(String dataDate) {
		if(dataDate != null){
			this.dataDate = dataDate;
		}else{
			this.dataDate = "null";
		}	}

	public String getDataDesc() {
		return dataDesc;
	}

	public void setDataDesc(String dataDesc) {
		this.dataDesc = dataDesc;
	}
	
	
	
	
    

}
