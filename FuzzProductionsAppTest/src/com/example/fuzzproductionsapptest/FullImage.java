package com.example.fuzzproductionsapptest;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;


public class FullImage extends Activity {
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.full_image);
	
		ImageView fullImage = (ImageView) findViewById(R.id.fullImg);
	
		Bundle extras = getIntent().getExtras();
		byte[] b = extras.getByteArray("picture");

		Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);

		fullImage.setImageBitmap(bmp);
}
}	