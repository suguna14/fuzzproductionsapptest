package com.example.fuzzproductionsapptest;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class DataAdapter extends ArrayAdapter<DataModel>{
	
	public Context mContext = this.getContext();
	ArrayList<DataModel> dataList;
	LayoutInflater mInflater;
	int mPosition;
	ViewHolder mHolder;
	
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options = new DisplayImageOptions.Builder()
			.cacheInMemory(true)
			.resetViewBeforeLoading(true)
			.showImageOnFail(R.drawable.error404)
			.showImageOnLoading(R.drawable.loadingwait)
			.bitmapConfig(Bitmap.Config.ARGB_4444)
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.build();
	
		
	public DataAdapter(Context context, int position, ArrayList<DataModel> objects) {
		super(context, position, objects);
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mPosition = position;
		dataList = objects;
	}
 
	
	@Override
	public View getView(int position, View holderView, ViewGroup parent) {
		// convert view = design
		View view = holderView;
		if (view == null) {
			mHolder = new ViewHolder();
			view = mInflater.inflate(mPosition, null);
			
			mHolder.imageView = (ImageView)view.findViewById(R.id.dataImg);
			mHolder.dataId = (TextView) view.findViewById(R.id.dataId);
			mHolder.dataType = (TextView) view.findViewById(R.id.dataType);
			mHolder.dataDate = (TextView) view.findViewById(R.id.dataDate);
			mHolder.dataDesc = (TextView) view.findViewById(R.id.dataDesc);
			view.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) view.getTag();
		}
		
		mHolder.dataId.setText(dataList.get(position).getDataId());
		mHolder.dataType.setText(dataList.get(position).getDataType());
		mHolder.dataDate.setText(dataList.get(position).getDataDate());
		mHolder.imageView.setVisibility(2);
		mHolder.dataDesc.setText(dataList.get(position).getDataDesc());
		try{
			mHolder.imageView.setVisibility(0);
			mHolder.imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			mHolder.imageView.setAdjustViewBounds(true);
			imageLoader.displayImage(dataList.get(position).getDataImg(), mHolder.imageView, options);
		
		}catch(Exception e ){
			Log.d("imageLoader error", "" +e.getMessage());
		}

		
		return view;

	}

	
	static class ViewHolder {
		public ImageView imageView;
		public TextView dataId;
		public TextView dataType;
		public TextView dataDate;
		public TextView dataDesc;
		public ImageView dataImg;
	}


}
	




	
	
	
	
